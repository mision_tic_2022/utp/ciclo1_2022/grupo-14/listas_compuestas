

hijo_1 = ['Nestor', 'Alejandra']
hijo_2 = ['Tatiana', 'Ricardo']

padre = [hijo_1, hijo_2]

nombre_1 = padre[0][0]
print(nombre_1)

nombre_2 = padre[0][1]
print(nombre_2)

nombre_3 = padre[1][0]
print(nombre_3)

nombre_4 = padre[1][1]
print(nombre_4)

print('----------------------')

for hijo in padre:
    print(hijo)
    for n in hijo:
        print(n)