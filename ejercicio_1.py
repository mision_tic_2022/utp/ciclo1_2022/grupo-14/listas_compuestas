padre = [
    [300, 150, 80],
    [400, 500, 600],
    [10, 20, 30],
    [40, 50, 60]
]

'''
Desarrollar una función que reciba como parámetro al lista 'padre'.
Retorne la sumatoria TOTAL de los elementos de la lista padre.
'''

#Solución de Nestor
def sumar(padre):
    sumatoria = 0
    for hijo in padre:
        for n in hijo:
            sumatoria +=n
    return sumatoria