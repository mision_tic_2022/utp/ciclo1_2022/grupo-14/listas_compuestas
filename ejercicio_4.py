'''
1) Mostrar todos los productos por categoria
2) Buscar productos por categoría
NOTA:
    La información está contenida en una lista compuesta de dos filas.
    *La primera fila contiene 'n' categorías
    *La segunda fila contiene 'n' productos, cada producto está representado en un diccionario
'''

productos = [
    ["electronics","jewelery","men's clothing"],
    [ 
        [{'product': 'X', 'price': 3.4}, {'product': 'A', 'price': 3.4}], 
        [{'product': 'Y', 'price': 10.12}],
        [{'product': 'Z', 'price': 5.02}]
    ]
]



def agrupar_x_categoria(datos: list)->dict:
    respuesta = dict()
    categorias: list = datos[0]
    productos: list = datos[1]
    #Iterar categorias
    for nombre_categoria in categorias:
        index = categorias.index(nombre_categoria)
        respuesta[nombre_categoria] = productos[index]
    return respuesta

print(agrupar_x_categoria(productos))