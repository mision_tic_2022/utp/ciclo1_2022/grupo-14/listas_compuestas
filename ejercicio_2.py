
abuelo = [
    [
        [10,20,30,40],
        [40,50,60,70],
        [40,10,30,20]
    ],
    [
        [20,30,90,20],
        [30,40,80,50],
        [50,50,70,10]
    ]
]

'''
Desarrolle un función que reciba como parámetro la lista 'abuelo'.
Retorne un diccionario que contenga la sumatoria total de cada una 
de las listas 'padre' y la sumatoria total de la lista 'abuelo'
{
    'sumatoria_padres': [100, 200],
    'sumatoria_total': 2000
}
Donde 100 y 200 es la sumatoria de cada padre de la lista abuelo y sumatoria_total 
es la sumatoria de toda la lista 'abuelo'
'''
#Solución de Luis Alejandro
def diccionario_sumatoria(abuelo: list)->dict:
    sumatoria_total=0
    respuesta=dict()
    respuesta["sumatoria_padres"]=[]
    for padre in abuelo:
        sumatoria_padres=0
        for hijo in padre:
            for x in hijo:
                sumatoria_padres+=x
                sumatoria_total+=x
        respuesta["sumatoria_padres"].append(sumatoria_padres)
    respuesta["sumatoria_total"]=sumatoria_total
    return respuesta

print(diccionario_sumatoria(abuelo))

