'''
Desarrolle un software que registre n estudiantes y los almacene 
en una lista. Posteriormente debe permitir al usuario 
registrar las notas de cada uno de los estudiantes y estas 
almacenarlas dentro de otra lista.
ambas listas deben de estar en una lista compuesta. Ejemplo:
    [
        ['Juan', 'Alejandra']
        [4.5,   4,9]
    ]
La lista compuesta en la primera fila almacena los estudiantes y en la 
segunda fila almacena las notas de los estudiantes.
-----------------------
* 'Juan' | 'Alejandra'|
*----------------------
* 4.5    | 4.9        |
*----------------------
'''

curso = [
    ['Juan', 'Alejandra', 'Pedro'],
    [4.5, 4.9, 3.5]
]


def mostrar_notas(curso: list):
    for hijo in curso:
        print('--------------------------------------')
        cadena = '*'
        for element in hijo:
            cadena = cadena+'\t'+str(element)
        print(cadena)
        print('--------------------------------------')

#Solución de Luis Alejandro
def mostrar_nota_X_estudiante(curso: list):
    #Obtener nombre del estudiante
    nombre = input('Nombre del estudiante: ')
    nota = 0.0
    existe_estudiante=False
    for i in range(0,len(curso[0])):
        if(nombre==curso[0][i]):
            nota=curso[1][i]
            existe_estudiante=True
    if not existe_estudiante:
        nota="No existe el estudiante"
    return nota

""" def mostrar_nota_X_estudiante(curso: list):
    #Obtener nombre del estudiante
    nombre = input('Nombre del estudiante: ')
    nota = ''
    if nombre in curso[0]:
        index = curso[0].index(nombre)
        nota = curso[1][index]
    else:
        nota = "No existe el estudiante"
    
    return nota """


mostrar_notas(curso)